.PHONY: up down

up:
	minikube start --addons=ingress
	minikube kubectl -- apply -f app/

down:
	minikube delete
