# realFlowControl/php-k8s-simple

## Prerequisits

You need to have `docker` and `minikube` installed.

## Usage

```bash
$ make
```

To access the PHP container find out the IP address of the ingress.

```bash
$ minikube kubectl -- get ingress php-ingress
NAME          CLASS    HOSTS   ADDRESS        PORTS   AGE
php-ingress   <none>   *       192.168.49.2   80      33m
```

Direct your browser to `http://192.168.49.2/php-test/` (change IP address
according to your ingress IP address).
